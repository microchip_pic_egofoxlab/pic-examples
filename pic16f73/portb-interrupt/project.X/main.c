#include <xc.h>
#include <pic16f73.h>

// CONFIG
#pragma config FOSC = HS        // Oscillator Selection bits (HS oscillator)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = ON       // Power-up Timer Enable bit (PWRT enabled)
#pragma config CP = OFF         // FLASH Program Memory Code Protection bit (Code protection off)
#pragma config BOREN = ON       // Brown-out Reset Enable bit (BOR enabled)

//	Set Frequency
#define _XTAL_FREQ 12000000;

#define HIGH 1;
#define LOW 0;

unsigned char PORB_LastInfo;

void main(void) {
	TRISC0 = LOW;

	RBIE = HIGH;
	RBIF = LOW;
	GIE = HIGH;

	while (1) {
	}
}

//	ISR - Interrupt Source Routine
void interrupt ISR() {
	//	If PortB interrupt occurred
	if (RBIE & RBIF) {
		//	important!; You need read PORTB data
		PORB_LastInfo = PORTB;
		RBIF = LOW;
		
		//	Toggle LED
		RC0 ^= 1;
	}
}
