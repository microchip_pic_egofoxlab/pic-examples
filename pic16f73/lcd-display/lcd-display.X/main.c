#include <xc.h>
#include <pic16f73.h>

// CONFIG
#pragma config FOSC = HS        // Oscillator Selection bits (HS oscillator)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = ON       // Power-up Timer Enable bit (PWRT enabled)
#pragma config CP = OFF         // FLASH Program Memory Code Protection bit (Code protection off)
#pragma config BOREN = ON       // Brown-out Reset Enable bit (BOR enabled)

#define _XTAL_FREQ 12000000

#define HIGH 1
#define LOW 0

#define RS RB0
#define EN RB1
#define D4 RB2
#define D5 RB3
#define D6 RB4
#define D7 RB5

#include "my-lcd.h"

void startLed(void) {
	for (char i = 0; i <= 5; i++) {
		__delay_ms(70);
		RC0 ^= HIGH;
	}
}

void main(void) {
	TRISB0 = LOW;
	TRISB1 = LOW;
	TRISB2 = LOW;
	TRISB3 = LOW;
	TRISB4 = LOW;
	TRISB5 = LOW;
	TRISC0 = LOW;
	
	startLed();
	
	lcdInit();
	
	while (1) {
		lcdClear();
        __delay_ms(1000);
		
		lcdSetCursor(1, 1);
		lcdWriteString("Line 1");
		
		__delay_ms(1000);
		lcdSetCursor(2, 4);
		lcdWriteString("Line 2");
		
        
        for (int i = 0; i < 32; i++) {
            lcdShiftScreenRight();
            __delay_ms(300);
        }
        
		__delay_ms(500);
		RC0 ^= HIGH;
	}
}
