#ifndef XC_HEADER_TEMPLATE_H
#define	XC_HEADER_TEMPLATE_H

#include <xc.h>

#endif	/* XC_HEADER_TEMPLATE_H */

#ifndef RS
#define RS RB0
#define EN RB1
#define D4 RB2
#define D5 RB3
#define D6 RB4
#define D7 RB5
#endif

/**
 * Стробирующий импульс
 */
void lcdStrobe() {
	EN = 1;
	__delay_ms(2);
	EN = 0;
}

/**
 * Воспроизвести одну тетраду на выходах
 * @param a
 */
void lcdPort(char a) {
    if (a & 1)
        D4 = 1;
    else
        D4 = 0;

    if (a & 2)
        D5 = 1;
    else
        D5 = 0;

    if (a & 4)
        D6 = 1;
    else
        D6 = 0;

    if (a & 8)
        D7 = 1;
    else
        D7 = 0;
}

/**
 * Передает команду ЖКИ. Сначала передает малдшую, потом старшую тетраду
 * @param cmd - Шестнадцатеричная команда
 */
void lcdCmd(char cmd) {
    char x, y;
    x = cmd & 0x0f;
    y = cmd >> 4;
    
    RS = 0;
            
    lcdPort(y);
    lcdStrobe();
    __delay_ms(2);
    
    lcdPort(x);
    lcdStrobe();
    __delay_ms(2);
}

/**
 * Инициализация LCD. Ориентировался на 1602A. У вас может быть другая 
 * инициализация. Читайте datasheet по LCD
 */
void lcdInit(void) {
	__delay_ms(1000);
    lcdCmd(0x00);
    __delay_ms(20);
	
    lcdCmd(0x02);
	lcdCmd(0x02);
    lcdCmd(0x0C);
    
	__delay_ms(50);
	
    lcdCmd(0x00);
	lcdCmd(0x0F);
   
	__delay_ms(50);
	
    lcdCmd(0x00);
    lcdCmd(0x01);
	
	__delay_ms(2);
    
	lcdCmd(0x00);
    lcdCmd(0x06);
}

/**
 * Очищает дисплей
 */
void lcdClear() {
    lcdCmd(0x01);
    lcdCmd(0x00);
}

/**
 * Пишет одну букву
 * @param a - код буквы
 */
void lcdWriteChar(char a) {
    RS = 1;
    
    char x, y;
    x = a & 0x0f;
    y = a >> 4;
    
    lcdPort(y);
    lcdStrobe();
    lcdPort(x);
    lcdStrobe();
}

/**
 * Пишет заданную строку
 * @param a
 */
void lcdWriteString(char *a) {
    int i;
    
    for (i = 0; a[i] != '\0'; i++)
        lcdWriteChar(a[i]);
}

/**
 * Устанавливает курсор в указанную позицию
 * @param a
 * @param b
 */
void lcdSetCursor(char a, char b) {
    char temp;
    
    if (a == 1) {
        temp = 0x80 + b;
    } else if (a == 2) {
        temp = 0xC0 + b;
    }
    
    lcdCmd(temp);
}

/**
 * Сдвигает текст вправо
 */
void lcdShiftScreenRight() {
    lcdCmd(0x1c);
}

/**
 * Сдвигает текст влево
 */
void lcdShiftScreenLeft() {
    lcdCmd(0x1);
}
