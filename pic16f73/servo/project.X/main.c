#include <xc.h>
#include "pic16f73.h"

// CONFIG
#pragma config FOSC = HS        // Oscillator Selection bits (HS oscillator)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = OFF      // Power-up Timer Enable bit (PWRT disabled)
#pragma config CP = OFF         // FLASH Program Memory Code Protection bit (Code protection off)
#pragma config BOREN = OFF      // Brown-out Reset Enable bit (BOR disabled)

#define _XTAL_FREQ 12000000
#define _1_US (_XTAL_FREQ/1000000)
#define HIGH 1
#define LOW 0

#define __my_delay_us(x) _delay((unsigned long)((x)*(_XTAL_FREQ/4000000.0)))

void Init();
long int Servo_CalcDeg(int deg);

void delay_us(unsigned long delay) {
    delay = delay / _1_US;
    
    for (unsigned long i = 0; i < delay; i++) {
    }
}

void main(void) {
    Init();
    
    int counter = 0;
    unsigned long degDelay = 2000.0;
    
    while (HIGH) {
        __delay_ms(20);
        RA0 = HIGH;
        
        if (counter == 120) {
            counter = 0;
        }
        
        if (counter < 60) {
            delay_us(800);
        }
        
        if (counter > 60) {
            delay_us(2200);
        }
        
        RA0 = LOW;
        
        counter++;
    }
}

void Init() {
    //  Output for servo
    ADCON1 = 0xff;
    TRISA0 = LOW;

    TRISC0 = LOW;
    TRISC1 = LOW;
}

long int Servo_CalcDeg(int deg) {
    int delayDeg0 = 600;
    int delayDeg180 = 2000;
    int step = (delayDeg180 - delayDeg0) / 180;

    return delayDeg0 + step * deg;
}
