#include <xc.h>

#ifndef RS
#define RS RB0
#define EN RB1
#define D4 RB2
#define D5 RB3
#define D6 RB4
#define D7 RB5
#endif

/**
 * Стробирующий импульс
 */
void lcdStrobe();

/**
 * Воспроизвести одну тетраду на выходах
 * @param a
 */
void lcdPort(char a);

/**
 * Передает команду ЖКИ. Сначала передает малдшую, потом старшую тетраду
 * @param cmd - Шестнадцатеричная команда
 */
void lcdCmd(char cmd);

/**
 * Инициализация LCD. Ориентировался на 1602A. У вас может быть другая 
 * инициализация. Читайте datasheet по LCD
 */
void lcdInit(void);

/**
 * Очищает дисплей
 */
void lcdClear();

/**
 * Пишет одну букву
 * @param a - код буквы
 */
void lcdWriteChar(char a);

/**
 * Пишет заданную строку
 * @param a
 */
void lcdWriteString(char *a);

/**
 * Устанавливает курсор в указанную позицию
 * @param a
 * @param b
 */
void lcdSetCursor(char a, char b);

/**
 * Сдвигает текст вправо
 */
void lcdShiftScreenRight();

/**
 * Сдвигает текст влево
 */
void lcdShiftScreenLeft();

/**
 * Clear screen and print text from the beginning
 * 
 * @param text
 */
void lcdEcho(char *text);
