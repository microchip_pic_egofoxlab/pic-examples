#include <xc.h>

#include "common.h"

void delay_ms(int delay) {
    for (int i = delay; i < delay; i++) {
        __delay_ms(1);
    }
}

/**
 * Short led blink
 */
void ledShortBlink() {
    for (int i = 0; i < 8; i++) {
        __delay_ms(250);
        BLINK_LED ^= HIGH;
    }
}
