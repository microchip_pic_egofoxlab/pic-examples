#include <xc.h>
#include <pic16f73.h>

// CONFIG
#pragma config FOSC = HS        // Oscillator Selection bits (HS oscillator)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = OFF      // Power-up Timer Enable bit (PWRT disabled)
#pragma config CP = OFF         // FLASH Program Memory Code Protection bit (Code protection off)
#pragma config BOREN = OFF      // Brown-out Reset Enable bit (BOR disabled)

#define _XTAL_FREQ 20000000

#include "common.h"

#define RS RB0
#define EN RB1
#define D4 RB2
#define D5 RB3
#define D6 RB4
#define D7 RB5

#include <string.h>
#include "lcd.h"
#include "usart.h"
#include "esp8266.h"

#define BLINK_LED RC1

/**
 * Program initialization
 */
void Init(void);

/**
 * Server response callback
 * 
 * @param response
 */
void serverResponseCallback(char *response);

void main(void) {
    Init();
    lcdInit();
    lcdClear();

    Initialize_ESP8266();

    do {
        lcdEcho("ESP Not Found");
    } while (!esp8266_isStarted());

    RC1 = HIGH;
    lcdEcho("ESP Connected!");

    __delay_ms(500);

    if (esp8266_startConnection("192.168.4.100", "80") == true) {
        lcdEcho("Conn success");
    } else {
        lcdEcho("Conn failed");
    }

    serverResponseCallback(esp8266_sendData("ECHO"));

    while (true) {
        serverResponseCallback(esp8266_readServerResponse());
    }
}

void Init() {
    //  LCD display output
    TRISB0 = LOW;
    TRISB1 = LOW;
    TRISB2 = LOW;
    TRISB3 = LOW;
    TRISB4 = LOW;
    TRISB5 = LOW;

    //  Led indicator
    TRISC0 = LOW;
    TRISC1 = LOW;
    RC0 = LOW;
    RC1 = LOW;

    for (int i = 0; i < 5; i++) {
        __delay_ms(250);
        RC0 ^= HIGH;
    }

    RC0 = HIGH;
}

void serverResponseCallback(char *response) {
    if (strcmp(response, "BLINK") == 0) {
        ledShortBlink();
    }
}
