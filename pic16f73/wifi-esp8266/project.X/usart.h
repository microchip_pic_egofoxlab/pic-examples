#include <stdint.h>
#include <stdbool.h>

/**
 * Initialize USART
 * 
 * @param baudRate Baud rate size
 * @param brgh High or low speed
 */
void usartInit(unsigned long int baudRate, bool brgh);

/**
 * Return the readiness of incoming data
 * 
 * @return 
 */
char usartDataReady();

/**
 * Read and return received data
 * @return 
 */
char usartDataRead();

void usartReadText(char *Output, unsigned int length);

char usartTxEmpty();

void usartWrite(char data);

void usartWriteText(char *text);
