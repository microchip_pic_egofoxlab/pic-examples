#include <xc.h>

#include <stdbool.h>
#include <string.h>
#include "common.h"
#include "usart.h"
#include "lcd.h"
#include "esp8266.h"

#ifndef _XTAL_FREQ
#define _XTAL_FREQ 20000000
#endif

#define SHOW_TIME 1500
#define DEBUG false

char tmpResponse[12];
char serverResponse[12];

void Initialize_ESP8266() {
    //  Initialize USART
    usartInit(115200, true);

    //  Important correct SPBRG
    SPBRG = 10;
    //
    //    TRISC6 = 0; // TX Pin set as output
    //    TRISC7 = 1; // RX Pin set as input
    //    //________I/O pins set __________//
    //
    //    /**Initialize SPBRG register for required 
    //    baud rate and set BRGH for fast baud_rate**/
    //    SPBRG = 10;
    //    BRGH = 1; // for high baud_rate
    //    //_________End of baud_rate setting_________//
    //
    //    //****Enable Asynchronous serial port*******//
    //    SYNC = 0; // Asynchronous
    //    SPEN = 1; // Enable serial port pins
    //    //_____Asynchronous serial port enabled_______//
    //    //**Lets prepare for transmission & reception**//
    //    TXEN = 1; // enable transmission
    //    CREN = 1; // enable reception
    //    //__UART module up and ready for transmission and reception__//
    //
    //    //**Select 8-bit mode**//  
    //    TX9 = 0; // 8-bit reception selected
    //    RX9 = 0; // 8-bit reception mode selected

    usartWriteText("ATE0\r\n");
    esp8266_readResponse();
}

bool esp8266_isStarted() {
    usartWriteText("AT\r\n");

    if (esp8266_readResponse() == ESP_OK) {
        return true;
    }

    lcdClear();
    lcdSetCursor(1, 0);
    lcdWriteString(tmpResponse);
    __delay_ms(2000);

    return false;
}

char esp8266_sendCommand(const char *command) {
    if (DEBUG) {
        lcdEcho(command);
        __delay_ms(SHOW_TIME);
    }

    usartWriteText(command);
    return esp8266_readResponse();
}

bool esp8266_startConnection(const char *ip, const char *port) {
    char response;

    if (esp8266_sendCommand("AT+CIPMODE=0\r\n") != ESP_OK) {
        return false;
    }

    if (esp8266_sendCommand("AT+CIPMUX=1\r\n") != ESP_OK) {
        return false;
    }

    response = esp8266_sendCommand("AT+CIPSTART=0,\"TCP\",\"192.168.0.103\",7000\r\n");
    while (true) {
        if (response == ESP_OK || response == ESP_ERROR) {
            return response == ESP_OK;
        }

        response = esp8266_readResponse();
    }
}

char *esp8266_sendData(char *data) {
    usartWriteText("AT+CIPSEND=0,4\r\n");

    while (true) {
        if (usartDataRead() == 0x20) {
            break;
        }
    }

    usartWriteText(data);
    
    return esp8266_readServerResponse(serverResponse);
}

void esp8266_readServerResponse_(char *response) {
    char responseCnt = 0;
    char received;
    
    while (true) {
        received = usartDataRead();

        if (received == 0x3a) {
            RC0 ^= HIGH;
            while (true) {
                received = usartDataRead();

                if (received <= 0x0d && usartDataRead() == 0x0a) {
                    break;
                }

                response[responseCnt] = received;
                responseCnt++;
            }

            break;
        }
    }
}

char *esp8266_readServerResponse() {
    for (char i = 0; i < 12; i++) {
        serverResponse[i] = '\0';
    }

    esp8266_readServerResponse_(serverResponse);
    
    return serverResponse;
}

char esp8266_readResponse() {
    char so_far[2] = {0, 0};
    const char lengths[2] = {2, 5};
    const char* strings[2] = {"OK", "ERROR"};
    const char responses[2] = {ESP_OK, ESP_ERROR};
    char response[16];

    for (char i = 0; i < 16; i++) {
        response[i] = '\0';
    }

    char responseCnt = 0;
    char received;
    char responseCode;
    char responseI;
    bool continue_loop = true;

    while (continue_loop) {
        received = usartDataRead();

        if (received == 0x0a || received == 0x0d) {
            for (char i = 0; i < 16; i++) {
                response[i] = '\0';
            }

            responseCnt = 0;
        } else {
            response[responseCnt] = received;
            responseCnt++;
        }

        for (unsigned char i = 0; i < 2; i++) {
            if (strings[i][so_far[i]] == received) {
                so_far[i]++;
                if (so_far[i] == lengths[i]) {
                    //if (strcmp(strings[i], response) == 0) {
                    responseCode = responses[i];
                    responseI = i;
                    continue_loop = false;
                    break;
                    //}
                }
            } else {
                so_far[i] = 0;
            }
        }
    }

    if (DEBUG) {
        lcdEcho(response);
        __delay_ms(SHOW_TIME);
    }
    
    return responseCode;
}
