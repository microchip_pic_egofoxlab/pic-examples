#include <xc.h>

#include <stdint.h>
#include <stdbool.h>
#include <math.h>
#include "common.h"

#ifndef _XTAL_FREQ
#define _XTAL_FREQ 20000000
#endif

/**
 * Initialize USART
 * 
 * @param baudRate Baud rate size
 * @param brgh High or low speed
 */
void usartInit(unsigned long int baudRate, bool brgh) {
    //  High speed
    if (brgh) {
        SPBRG = (int) (_XTAL_FREQ / (16 * baudRate) - 1);
        BRGH = HIGH;
    } else {//  Low speed
        SPBRG = (int) (_XTAL_FREQ / (64 * baudRate) - 1);
        BRGH = LOW;
    }

    //  Transmitted
    TXEN = HIGH;
    SYNC = LOW;

    //  Receive
    SPEN = HIGH;
    CREN = HIGH;
    TXREG = 0x0;

    TRISC7 = HIGH;
    TRISC6 = HIGH;
}

/**
 * Return the readiness of incoming data
 * 
 * @return 
 */
char usartDataReady() {
    return RCIF;
}

/**
 * Read and return received data
 * @return 
 */
char usartDataRead() {
    while (!usartDataReady());

    return RCREG;
}

void usartReadText(char *Output, unsigned int length) {
    unsigned int i;
    
    for (int i = 0; i < length; i++)
        Output[i] = usartDataRead();
}

char usartTxEmpty() {
    return TRMT;
}

void usartWrite(char data) {
    while (!TRMT);
    
    TXREG = data;
}

void usartWriteText(char *text) {
    for (int i = 0; text[i] != '\0'; i++) {
        usartWrite(text[i]);
    }
}
