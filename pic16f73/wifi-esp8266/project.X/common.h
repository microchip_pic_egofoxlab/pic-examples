#include <xc.h>

#ifndef _XTAL_FREQ
#define _XTAL_FREQ 20000000
#endif

#ifndef HIGH
#define HIGH 1
#endif

#ifndef LOW
#define LOW 0
#endif

#ifndef BLINK_LED
#define BLINK_LED RC1
#endif

void delay_ms(int delay);

/**
 * Short led blink
 */
void ledShortBlink();
