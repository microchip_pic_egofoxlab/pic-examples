#include <stdbool.h>

#define ESP_UNKNOWN -1
#define ESP_OK 1
#define ESP_ERROR 2

void Initialize_ESP8266();

bool esp8266_isStarted();

char esp8266_sendCommand(const char *command);

bool esp8266_startConnection(const char *ip, const char *port);

char *esp8266_sendData(char *data);

char *esp8266_readServerResponse();

void esp8266_readServerResponse_(char *response);

char esp8266_readResponse();
