#include <xc.h>


// CONFIG
#pragma config FOSC = HS        // Oscillator Selection bits (HS oscillator)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = ON       // Power-up Timer Enable bit (PWRT enabled)
#pragma config CP = OFF         // FLASH Program Memory Code Protection bit (Code protection off)
#pragma config BOREN = ON       // Brown-out Reset Enable bit (BOR enabled)

#define _XTAL_FREQ 12000000

#define HIGH 1
#define LOW 0

void main(void) {
	//	LED indicators
	TRISC = 0b11000000;
	
	//	Indicate that microcontroller is worked
	RC0 = HIGH;
	
	//	Read PORTA pins status
	//	Set input port as digital
	PCFG0 = HIGH;
	PCFG1 = HIGH;
	PCFG2 = HIGH;
	TRISA = 0xff;
	
	while(HIGH) {
		PORTC = PORTA;
	}
}
