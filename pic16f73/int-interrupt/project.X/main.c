#include <xc.h>
#include <pic16f73.h>

// CONFIG
#pragma config FOSC = HS        // Oscillator Selection bits (HS oscillator)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = OFF       // Power-up Timer Enable bit (PWRT enabled)
#pragma config CP = OFF         // FLASH Program Memory Code Protection bit (Code protection off)
#pragma config BOREN = ON       // Brown-out Reset Enable bit (BOR enabled)

#define _XTAL_FREQ 12000000

#define HIGH 1;
#define LOW 0;

void Init(void);

void main(void) {
	Init();
	
	while (1);
}

void Init(void) {
	//	Global interrupt
	GIE = HIGH;
	//	External interrupt enabled
	INTE = HIGH;
	//	Clear external interrupt flag
	INTF = LOW;
	
	//	Set interrupt occurred on falling edge
	INTEDG = LOW;
	
	//	PortC 0 and 1 set output
	TRISC0 = LOW;
	TRISC1 = LOW;
	
	RC0 = LOW;
	RC1 = HIGH;
} 

//	ISR - Interrupt Service Routine
void interrupt ISR() {
	if (INTE & INTF) {
		//	Clear interrupt flag
		INTF = LOW;
		
		RC0 ^= HIGH;
		RC1 ^= HIGH;
	}
}
